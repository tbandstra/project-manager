<?php

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;

class NodeControllerTest extends ControllerTest
{
    private $url = '/nodes';

    public function testGetNodesWithoutQueryParameter()
    {
        $this->client->request('GET', $this->url);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testGetNodesWhitGivenJwtToken()
    {
        $this->client->request('GET', $this->url, [], [], $this->headers);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testGetNodesDash()
    {
        $this->client->request('GET', '/nodes?uuid=' . $this->uuid, [], [], $this->headers);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertCount(10, $content);
    }

    public function testAddNodeWithoutAuthentication()
    {
        $this->headers['HTTP_Authorization'] = '';
        $this->client->request('POST', $this->url, [], [], $this->headers, json_encode($this->getNodeData()));
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    private function getNodeData()
    {
        return [
            'name' => 'test add node',
            'longitude' => '52.375',
            'latitude' => '4.887',
            'device_id' => 'ttn-device-id-4'
        ];
    }

    public function testAddNode()
    {
        $this->login();
        $this->client->request('POST', $this->url, [], [], $this->headers, json_encode($this->getNodeData()));
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertEquals('test add node', $content['data']['name']);
    }
}
