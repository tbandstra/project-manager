<?php

namespace App\Tests\Controller;

use App\Entity\Network;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;

class ProjectControllerTest extends ControllerTest
{
    private $url = '/projects';

    public function testGetProjects()
    {
        $this->client->request('GET', $this->url . '?uuid=' . $this->uuid, [], [], $this->headers);
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(1, $content);
        $this->assertEquals('Pest Control', $content[0]['name']);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testGetProjectsWithoutAuthentication()
    {
        $this->client->request('GET', $this->url);
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('uuid not available as query parameter', $content['error']);
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testCreateProject()
    {
        $this->login();
        $network = new Network();
        $network->setName('The Things Network');
        $network->setCode('ttn');
        $this->em->persist($network);
        $this->em->flush();
        $json = '{"name":"Project Naam","network":"ttn","access_key":"ttn-account-v2.D9IVdeGOkfqYDyzczutvzSEDCYO-vTna5cu9M8cNy1U"}';
        $this->client->request('POST', $this->url, [], [], $this->headers, $json);
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertEquals(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
    }
}
