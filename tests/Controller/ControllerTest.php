<?php

namespace App\Tests\Controller;

use Doctrine\ORM\EntityManager;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ControllerTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    /** @var Client */
    protected $client;
    /** @var EntityManager */
    protected $em;

    /** @var string One year valid */
    protected $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NTQ0NTc1MzgsInJvbGVzIjpbIlJPTEVfQURNSU4iLCJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJ0aW5lQGJhbmRzdHJhLmV1IiwiaXAiOiIxNzIuMTguMC40IiwidXVpZCI6ImYzMGUyYTA4LTRhNWYtMTFlOS05NjkxLTAyNDJhYzFlMDAwNiIsImV4cCI6MTU4NTk5MzUzOH0.LGPdIgIzwrXL_0lVryvKn5vRr-cbPhX42CZ2WOgeum98Nbp1a17BoIrBONq9GYbuGmsfNlrPaeSo65jcbZN7TEJtvVSNAPm170Ho_P5T4SJPoat2bu7CJ4PYP7E3k0AnfX0YBzOShiFFkMU3CrdFmHTO6YdyS-ogQ1Wo9-E0-lyhtBhTeZfAFu-bOWXcIZzxevfNWF59zVPX9jwF_KKTgZEm4zz9qCiJtQMRTDJy3HPJbzac1FQ6OmjhzcQctZ6DxpRtqaX5BooZHN97U22JKJkqo9-7pBfTID8v1x7Or4sX8St5iVCKAdqB6wmBt7W5VrZ9A67tkQYvMd279_vaCduqSUxEXGyfuoyL3VhOU_qw1nyS4UZEMLy9kR-iD1wEVDKXn8ucP0lj8n3qeUXMBLPjGwtqb1UzaY5koJS2KvOPYBwU0Bg7NGC5RKkrrtmFi1HkhmzcQhBIF8JYPbrymtih7S-bSSqaUETt7Jj-Bm2GW7r3tlhYNM1oJcuOonyNt3QP_q43ANSCFBSl-nQCweivAr3AZmtnkJa4W3H9jIRo2239J_vjdtJznNFM3bRF-MvLAGDlKRMpKjnr9GIMyg5frTVpTqjqGjIo_sfvSqmHjZ-rQMPfODJMxQ93ur_SARWyPKJwlDzOXBbRc_Ydvq1QMa4QiyN6EsML0mSVgzA';

    protected $uuid = 'f30e2a08-4a5f-11e9-9691-0242ac1e0006';

    protected $headers = [
        'CONTENT_TYPE' => 'application/json'
    ];

    public function setUp()
    {
        static::bootKernel(['environment' => 'test']);
        $this->client = self::$kernel->getContainer()->get('test.client');
        $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function login()
    {
        $this->headers['HTTP_Authorization'] = 'Bearer ' . $this->token;
    }

    public function testTest()
    {
        $this->assertTrue(true);
    }
}
