<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class Controller extends AbstractController
{
    /** @var array */
    protected $context;

    public function __construct()
    {
        $this->context =
            [
                'ignored_attributes' => [
                    '__initializer__',
                    '__cloner__',
                    '__isInitialized__',
                ],
                'circular_reference_handler' => function ($obj) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    return $obj->getId();
                },
            ];
    }

    protected function getProject(User $user = null): Project
    {
        if ($user === null) {
            $user = $this->getUser();
        }

        return $project = $user->getProjects()->first(); // for now
    }

    protected function getUser($uuid = null): User
    {
        $token = $this->get('security.token_storage')->getToken();
        if (null === $token) {
            return $this->getUserFromPayload($uuid);
        }
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findOneBy(['uuid' => $token->getAttribute('uuid')]);

        return $user;
    }

    /**
     * @param string $queryString
     * @return User|null
     * @throws Exception
     */
    protected function getUserFromQueryString($queryString): ?User
    {
        $data = explode('=', $queryString);
        if (count($data) !== 2 || $data[0] !== 'uuid') {
            throw new Exception('uuid not available as query parameter');
        }

        return $this->getUser($data[1]);
    }

    protected function getUserFromPayload($uuid): User
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findOneBy(['uuid' => $uuid]);

        return $user;
    }
}
