<?php

namespace App\Controller;

use App\Entity\Node;
use App\Service\NodeManager;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NodeController extends Controller
{
    private $nodeManager;
    private $project;

    public function __construct(NodeManager $nodeManager)
    {
        parent::__construct();
        $this->nodeManager = $nodeManager;
    }

    private function initProject()
    {
        $this->project = $this->getProject();
        $this->nodeManager->setProject($this->project);
    }

    public function dash(Request $request)
    {
        try {
            $nodeRepository = $this->getDoctrine()->getRepository(Node::class);

            $user = $this->getUserFromQueryString($request->getQueryString());
            $nodes = $nodeRepository->findBy(['project_id' => $this->getProject($user)->getId()]);

            return $this->json($nodes, Response::HTTP_OK, [], $this->context);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_FORBIDDEN, [], $this->context);
        }
    }

    public function store(Request $request)
    {
        $this->initProject();

        $data = json_decode($request->getContent(), true);
        $data['deviceId'] = $this->nodeManager->generateDeviceId();

        $this->nodeManager->storeAtNetwork($data);

        $node = new Node();
        $node->setName($data['name']);
        $node->setProject($this->project);
        $node->setLatitude($data['latitude']);
        $node->setLongitude($data['longitude']);
        $node->setDeviceId($data['deviceId']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($node);
        $em->flush();

        return $this->json([
            'message' => sprintf('Saved new node with device id %s', $node->getDeviceId()),
            'data' => $node
        ], Response::HTTP_CREATED, [], $this->context);
    }

    public function update(Request $request)
    {
        $this->initProject();
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $nodeRepository = $this->getDoctrine()->getRepository(Node::class);
        $i = 0;

        try {
            $this->nodeManager->setDevices();

            foreach ($data as $item) {
                /** @var Node $node */
                $node = $nodeRepository->findOneBy(['device_id' => $item['deviceId']]);
                if (empty($node) || !$this->locationIsChanged($node, $item)) {
                    continue;
                }

                $node->setLatitude($item['latitude']);
                $node->setLongitude($item['longitude']);
                $em->persist($node);

                $this->nodeManager->updateAtNetwork($item);
                $i++;
            }
            $em->flush();

            return $this->json(['message' => sprintf('Updated position of %d node(s)', $i),], Response::HTTP_OK, [], $this->context);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST, [], $this->context);
        }
    }

    public function delete(Request $request)
    {
        $this->initProject();
        $deviceId = $request->get('device_id');
        $em = $this->getDoctrine()->getManager();
        $nodeRepository = $this->getDoctrine()->getRepository(Node::class);

        try {
            /** @var Node $node */
            $node = $nodeRepository->findOneBy(['device_id' => $deviceId]);

            if (!$node) {
                throw new Exception('Node not found');
            }
            $em->remove($node);

            if ($this->nodeManager->deleteFromNetwork($deviceId)) {
                $em->flush();
            } else {
                throw new Exception('Error when removing from ttn network');
            }

            return $this->json(['message' => sprintf('Deleted node %s', $deviceId),], Response::HTTP_OK, [], $this->context);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST, [], $this->context);
        }
    }

    public function sync()
    {
        $this->initProject();
        try {
            $result = $this->nodeManager->syncDevices();
            if ($result) {
                return $this->json(
                    ['message' => sprintf('Synced nodes with ttn'),],
                    Response::HTTP_OK,
                    [],
                    $this->context
                );
            } else {
                throw new Exception('Error in syncing nodes with ttn network');
            }
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST, [], $this->context);
        }
    }

    /**
     * @param Node $node
     * @param array $data
     * @return bool
     */
    private function locationIsChanged(Node $node, $data): bool
    {
        if (isset($data['changed'])) {
            return $data['changed'] === true;
        }

        return ($node->getLatitude() != $data['latitude'] || $node->getLongitude() != $data['longitude']);
    }
}
