<?php

namespace App\Controller;

use App\Entity\Network;
use App\Entity\Project;
use App\Entity\TtnSettings;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{

    public function dash(Request $request)
    {
        try {
            $user = $this->getUserFromQueryString($request->getQueryString());
            $projects = $user->getProjects();

            return $this->json($projects, Response::HTTP_OK, [], $this->context);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_FORBIDDEN, [], $this->context);
        }
    }

    public function store(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        $networkRepository = $this->getDoctrine()->getRepository(Network::class);
        $network = $networkRepository->findOneBy(['code' => $data['network']]);
        if (empty($network)) {
            return $this->json(sprintf('network with id %s is not in the database', $data['network_id']), Response::HTTP_BAD_REQUEST);
        }
        $project = new Project();
        $project->setName($data['name']);
        $project->setNetwork($network);
        $project->addUser($this->getUser());
        $em->persist($project);

        $ttnSettings = new TtnSettings();
        $ttnSettings->setProject($project);
        $ttnSettings->setAccessKey($data['access_key']);
        $em->persist($ttnSettings);

        $em->flush();

        return $this->json(
            [
                'message' => sprintf('Saved new project with id %s', $project->getId()),
                'data' => $project
            ],
            Response::HTTP_CREATED, [], $this->context);
    }
}
