<?php

namespace App\Client;

use App\Entity\TtnSettings;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\HttpFoundation\Response;

class Ttn
{
    /** @var Client */
    private $client;
    /** @var Uri */
    private $base_uri;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->base_uri = $client->getConfig()['base_uri'];
    }

    /**
     * @param array $deviceData
     * @param TtnSettings $ttnSettings
     * @return bool
     * @throws Exception
     */
    public function putNodeTtn(array $deviceData, TtnSettings $ttnSettings): bool
    {
        $url = $this->getUrl($ttnSettings) . '/' . $deviceData['dev_id'];
        $response = $this->client->put(
            $url,
            [
                'body' => json_encode($deviceData),
                'headers' => [
                    'Authorization' => 'Key ' . $ttnSettings->getAccessKey(),
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $code = $response->getStatusCode();
        if ($code !== Response::HTTP_OK) {
            $reason = $response->getReasonPhrase();
            throw new Exception('Error in saving device info in ttn: ' . $code . ': ' . $reason);
        }

        return true;
    }

    public function postNodeTtn(array $data, TtnSettings $ttnSettings)
    {
        $url = $this->getUrl($ttnSettings);

        $bytes = random_bytes(8);
        $unique = strtoupper(bin2hex($bytes));
        $device = [
            'altitude' => 0,
            'app_id' => $ttnSettings->getApplicationId(),
            'attributes' => [
                'key' => '',
                'value' => '',
            ],
            'description' => $data['name'],
            'dev_id' => $data['deviceId'],
            'latitude' => (float)$data['latitude'],
            'longitude' => (float)$data['longitude'],
            'lorawan_device' => [
                'activation_constraints' => 'otaa',
                'app_eui' => $ttnSettings->getApplicationEui(),
                'app_id' => $ttnSettings->getApplicationId(),
                // 'app_key' => '',
                // 'app_s_key' => '03006D896D77455BC92A9FC61F619565',
                'dev_addr' => '01020304',
                'dev_eui' => $unique,
                'dev_id' => $data['deviceId'],
                'disable_f_cnt_check' => false,
                'f_cnt_down' => 0,
                'f_cnt_up' => 0,
                'last_seen' => 0,
//                'nwk_s_key' => '01020304050607080102030405060708',
                'uses32_bit_f_cnt' => true,
            ],
        ];

        $response = $this->client->post(
            $url,
            [
                'body' => json_encode($device),
                'headers' => [
                    'Authorization' => 'Key ' . $ttnSettings->getAccessKey(),
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $body = $response->getBody();
        $code = $response->getStatusCode(); // 200
        $reason = $response->getReasonPhrase(); // OK
    }

    /**
     * @param $deviceId
     * @param TtnSettings $ttnSettings
     * @return bool
     * @throws Exception
     */
    public function deleteNodeTtn($deviceId, TtnSettings $ttnSettings): bool
    {
        $url = $this->getUrl($ttnSettings) . '/' . $deviceId;
        $body = [
            'app_id' => $ttnSettings->getApplicationId(),
            'dev_id' => $deviceId
        ];
        $response = $this->client->delete(
            $url,
            [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Key ' . $ttnSettings->getAccessKey(),
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $code = $response->getStatusCode();
        if ($code !== Response::HTTP_OK) {
            $reason = $response->getReasonPhrase();
            throw new Exception('Error in removing device in ttn: ' . $code . ': ' . $reason);
        }

        return true;
    }

    private function getUrl(TtnSettings $ttnSettings)
    {
        $path = '/applications/' . $ttnSettings->getApplicationId() . '/devices';

        return $this->base_uri->withPath($path);
    }

    /**
     * @param TtnSettings $ttnSettings
     * @return array
     * @throws Exception
     */
    public function getDevices(TtnSettings $ttnSettings)
    {
        $url = $this->getUrl($ttnSettings);
        $response = $this->client->get(
            $url,
            [
                'headers' => [
                    'Authorization' => 'Key ' . $ttnSettings->getAccessKey(),
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        $code = $response->getStatusCode();
        if ($code !== Response::HTTP_OK) {
            $reason = $response->getReasonPhrase();
            throw new Exception('Error in getting devices from ttn: ' . $code . ': ' . $reason);
        }
        $body = (string)$response->getBody();
        $content = json_decode($body, true);

        return $content['devices'];
    }
}
