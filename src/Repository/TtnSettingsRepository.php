<?php

namespace App\Repository;

use App\Entity\TtnSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TtnSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method TtnSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method TtnSettings[]    findAll()
 * @method TtnSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TtnSettingsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TtnSettings::class);
    }

    // /**
    //  * @return TtnSettings[] Returns an array of TtnSettings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TtnSettings
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
