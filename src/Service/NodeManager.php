<?php

namespace App\Service;

use App\Client\Ttn;
use App\Entity\Node;
use App\Entity\Project;
use App\Entity\TtnSettings;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class NodeManager
{
    /** @var Project */
    private $project;
    private $ttnClient;
    private $entityManager;
    private $devices;

    public function __construct(Ttn $ttn, EntityManagerInterface $entityManager)
    {
        $this->ttnClient = $ttn;
        $this->entityManager = $entityManager;
    }

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @throws Exception
     */
    public function setDevices()
    {
        $this->devices = $this->ttnClient->getDevices($this->getSettings());
    }

    public function storeAtNetwork(array $data)
    {
        $networkCode = $this->project->getNetwork()->getCode();
        if ($networkCode !== 'ttn') {
            return;
        }

        $this->ttnClient->postNodeTtn($data, $this->getSettings());
    }

    /**
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function updateAtNetwork(array $data)
    {
        $networkCode = $this->project->getNetwork()->getCode();
        if ($networkCode !== 'ttn') {
            return false;
        }

        $deviceData = $this->findDeviceData($data['deviceId']);
        $deviceData['latitude'] = $data['latitude'];
        $deviceData['longitude'] = $data['longitude'];
        $deviceData['description'] = $data['name'];

        return $this->ttnClient->putNodeTtn($deviceData, $this->getSettings());
    }

    /**
     * @param $deviceId
     * @return bool
     * @throws Exception
     */
    public function deleteFromNetwork($deviceId): bool
    {
        $networkCode = $this->project->getNetwork()->getCode();
        if ($networkCode !== 'ttn') {
            return false;
        }

        return $this->ttnClient->deleteNodeTtn($deviceId, $this->getSettings());
    }

    /**
     * @param $deviceId
     * @return array
     * @throws Exception
     */
    private function findDeviceData($deviceId)
    {
        if (empty($this->devices)) {
            throw new Exception('No devices loaded');
        }

        foreach ($this->devices as $device) {
            if ($device['dev_id'] === $deviceId) {
                return $device;
            }
        }

        throw new Exception('No devices loaded');
    }

    private function getSettings()
    {
        $repos = $this->entityManager->getRepository(TtnSettings::class);

        return $repos->findOneBy(['project' => $this->project]);
    }

    /**
     * @return string
     */
    public function generateDeviceId(): string
    {
        return uniqid('ttn-device-');
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function syncDevices(): bool
    {
        $networkCode = $this->project->getNetwork()->getCode();
        if ($networkCode !== 'ttn') {
            return false;
        }

        $devices = $this->ttnClient->getDevices($this->getSettings());
        $nodeRepository = $this->entityManager->getRepository(Node::class);

        foreach ($devices as $device) {
            $node = $nodeRepository->findOneBy(['device_id' => $device['dev_id']]);
            if (!empty($node)) {
                continue;
            }

            $node = new Node();
            $node->setName(isset($device['description']) ? $device['description'] : 'empty name');
            $node->setProject($this->project);
            $node->setLatitude(isset($device['latitude']) ? $device['latitude'] : null);
            $node->setLongitude(isset($device['longitude']) ? $device['longitude'] : null);
            $node->setDeviceId($device['dev_id']);
            $this->entityManager->persist($node);
        }
        $this->entityManager->flush();

        return true;
    }
}
