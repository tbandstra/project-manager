<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TtnSettingsRepository")
 */
class TtnSettings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Project", inversedBy="ttnSettings", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $access_key;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $application_id;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $application_eui;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getAccessKey(): ?string
    {
        return $this->access_key;
    }

    public function setAccessKey(?string $access_key): self
    {
        $this->access_key = $access_key;

        return $this;
    }

    public function getApplicationId(): ?string
    {
        return $this->application_id;
    }

    public function setApplicationId(string $application_id): self
    {
        $this->application_id = $application_id;

        return $this;
    }

    public function getApplicationEui(): ?string
    {
        return $this->application_eui;
    }

    public function setApplicationEui(string $application_eui): self
    {
        $this->application_eui = $application_eui;

        return $this;
    }
}
